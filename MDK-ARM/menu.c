#include "main.h"
#include "menu.h"
#include  "can.h"
#include "oled.h"
#include "stm32f1xx_hal.h"
#include "can.h"
void (*current_operation_index)(); //执行当前操作函数
uint8_t func_index = 0;
uint8_t Get_key_next;
uint8_t Get_key_enter;


uint8_t TxData1[8]={0x2F,0x60,0x60,0x00,0x02,0x00,0x00,0x00};//设置模式为speed模式

uint8_t TxData2[8]={0x2B,0x42,0x60,0x00,0x2C,0x00,0x00,0x00};//设置速度位0xC8 = 200 rpm

uint8_t TxData3[8]={0x2B,0x40,0x60,0x00,0x06,0x00,0x00,0x00};//启动三联组
uint8_t TxData4[8]={0x2B,0x40,0x60,0x00,0x07,0x00,0x00,0x00};
uint8_t TxData5[8]={0x2B,0x40,0x60,0x00,0x0F,0x00,0x00,0x00};
uint8_t TxData6[8]={0x40,0x6C,0x60,0x00,0x00,0x00,0x00,0x00};
uint8_t TxData7[8]={0x40,0x39,0x20,0x02,0x00,0x00,0x00,0x00};
extern uint8_t Data_1810[8];
extern uint8_t Data_1811[8];
extern uint8_t RxData[8];
extern uint8_t Data_Speeed[8];
extern uint16_t num1;
extern char RxData_char[8];

extern uint16_t Hour;
extern uint8_t minute;
extern uint8_t second;
Menu_table table[26] =
    {
        {0, 0, 1, (*fun0)},  //一级界面
        {1, 2, 5, (*fun1)},  //二级菜单 can 模式
        {2, 3, 9, (*fun2)},  //二级菜单 485 模式
        {3, 4, 13, (*fun3)}, //二级菜单 串口模式
        {4, 1, 0, (*fun4)},  //二级菜单 Back

        {5, 6, 4, (*fun5)},  //三级菜单 Back
        {6, 7, 17, (*fun6)}, //三级菜单 Electric Info
        {7, 8, 18, (*fun7)}, //三级菜单 Alarm Info
        {8, 5, 19, (*fun8)}, //三级菜单 Vehicle Info

        {9, 10, 4, (*fun9)},    //三级菜单  Back
        {10, 11, 20, (*fun10)}, //三级菜单 Electric Info
        {11, 12, 21, (*fun11)}, //三级菜单 Alarm Info
        {12, 9, 22, (*fun12)},  //三级菜单 Vehicle Info

        {13, 14, 4, (*fun13)},  //三级菜单  Back
        {14, 15, 23, (*fun14)}, //三级菜单 Electric Info
        {15, 16, 24, (*fun15)}, //三级菜单 Alarm Info
        {16, 13, 25, (*fun16)}, //三级菜单 Vehicle Info

        {17, 6, 17, (*fun17)}, //四级菜单 空白
        {18, 7, 18, (*fun18)}, //四级菜单 空白
        {19, 8, 19, (*fun19)}, //四级菜单 空白

        {20, 10, 20, (*fun20)}, //四级菜单  空白
        {21, 11, 21, (*fun21)}, //四级菜单 空白
        {22, 12, 22, (*fun22)}, //四级菜单 空白

        {23, 14, 23, (*fun23)}, //四级菜单  空白
        {24, 15, 24, (*fun24)}, //四级菜单 空白
        {25, 16, 25, (*fun25)}, //四级菜单 空白

};

void Menu_key_set(void)
{
    Get_key_next = KEY2_StateRead();
    Get_key_enter = KEY1_StateRead();
    if (Get_key_next == KEY_DOWN)
    {
        func_index = table[func_index].next;
    }
    if (Get_key_enter == KEY_DOWN)
    {
        OLED_Clear();
        func_index = table[func_index].enter;
    }

    current_operation_index = table[func_index].current_operation;
    (*current_operation_index)(); //执行当前操作函数
    OLED_Refresh();
}
void meun1_func(void)
{
    OLED_ShowString(0, 0, "   1.CAN  Mode", 16, 1);
    OLED_ShowString(0, 17, "   2.485  Mode", 16, 1);
    OLED_ShowString(0, 33, "   3.UART Mode", 16, 1);
    OLED_ShowString(0, 49, "   4.BACK ", 16, 1);
}

void meun2_func(void)
{
    OLED_ShowString(0, 0, "   1.Back    ", 16, 1);
    OLED_ShowString(0, 17, "   2.Elect Info", 16, 1);
    OLED_ShowString(0, 33, "   3.Alarm Info", 16, 1);
    OLED_ShowString(0, 49, "   4.Vehicle Info", 16, 1);
}

void meun3_func(void)
{
    OLED_ShowString(0, 0, "   1.Back    ", 16, 1);
    OLED_ShowString(0, 17, "   2.Elect Info", 16, 1);
    OLED_ShowString(0, 33, "   3.Alarm Info", 16, 1);
    OLED_ShowString(0, 49, "   4.Vehicle Info", 16, 1);
}

void meun4_func(void)
{
    OLED_ShowString(0, 0, "   1.Back    ", 16, 1);
    OLED_ShowString(0, 17, "   2.Elect Info", 16, 1);
    OLED_ShowString(0, 33, "   3.Alarm Info", 16, 1);
    OLED_ShowString(0, 49, "   4.Vehicle Info", 16, 1);
}

void fun0(void) //
{
    LED1_TOGGLE;
    OLED_ShowString(0, 12, "RunTime:", 24, 1);
    OLED_ShowString(24, 40, ":  :", 24, 1);
    OLED_ShowString(25, 65, "num =", 16, 1);
    OLED_ShowNum(65, 66, num1, 4, 16, 1);
    OLED_ShowNum(73, 40, second, 2, 24, 1);
    OLED_ShowNum(37, 40, minute, 2, 24, 1);
    OLED_ShowNum(0, 40, Hour, 2, 24, 1);
}

void fun1(void)
{
    meun1_func();
    OLED_ShowString(0, 0, "-> ", 16, 1);
}

void fun2(void)
{
    meun1_func();
    OLED_ShowString(0, 17, "-> ", 16, 1);
}

void fun3(void)
{
    meun1_func();
    OLED_ShowString(0, 33, "-> ", 16, 1);
}

void fun4(void)
{
    meun1_func();
    OLED_ShowString(0, 49, "-> ", 16, 1);
}

void fun5(void)
{
    meun2_func();
    OLED_ShowString(0, 0, "-> ", 16, 1);
}

void fun6(void)
{
    meun2_func();
    OLED_ShowString(0, 17, "-> ", 16, 1);
    OLED_ShowString(0, 67, "            ", 16, 1);
}

void fun7(void)
{
    meun2_func();
    OLED_ShowString(0, 33, "-> ", 16, 1);
}

void fun8(void)
{
    meun2_func();
    OLED_ShowString(0, 49, "-> ", 16, 1);
}

void fun9(void)
{
    meun3_func();
    OLED_ShowString(0, 0, "-> ", 16, 1);
}

void fun10(void)
{
    meun3_func();
    OLED_ShowString(0, 17, "-> ", 16, 1);
}

void fun11(void)
{
    meun3_func();
    OLED_ShowString(0, 33, "-> ", 16, 1);
}

void fun12(void)
{
    meun3_func();
    OLED_ShowString(0, 49, "-> ", 16, 1);
}

void fun13(void)
{
    meun4_func();
    OLED_ShowString(0, 0, "-> ", 16, 1);
}

void fun14(void)
{
    meun4_func();
    OLED_ShowString(0, 17, "-> ", 16, 1);
}

void fun15(void)
{
    meun4_func();
    OLED_ShowString(0, 33, "-> ", 16, 1);
}

void fun16(void)
{
    meun4_func();
    OLED_ShowString(0, 49, "-> ", 16, 1);
}

//CAN can 空白
void fun17(void) // can 空白 第一行
{
    OLED_ShowString(0, 0, "  CAN Info1 ", 16, 1);
    OLED_ShowString(0, 16, "CAP=   AH", 16, 1);
    OLED_ShowString(0, 32, "SOC=   %", 16, 1);
    OLED_ShowString(0, 48, "Voltage=   V", 16, 1);
    OLED_ShowString(0, 64, "Current=   A", 16, 1);
    OLED_ShowNum(32, 16, (Data_1811[0] * 256 + Data_1811[1]) / 10, 3, 16, 1);
    OLED_ShowNum(32, 32, Data_1811[2], 3, 16, 1);
    OLED_ShowNum(64, 48, (Data_1810[0] * 256 + Data_1810[1]) / 10, 3, 16, 1);
    OLED_ShowNum(64, 64, (Data_1810[4] * 256 + Data_1810[5]) / 10, 3, 16, 1);
}

void fun18(void) // can 空白 第二行
{
    OLED_ShowString(0, 0, "  CAN Info2 ", 16, 1);
    OLED_ShowString(0, 16, "D0=     D1=   ", 16, 1);
    OLED_ShowNum(32, 16, Data_1811[0], 3, 16, 1);
    OLED_ShowNum(96, 16, Data_1811[1], 3, 16, 1);
    HAL_CAN_AddTxMessage(&hcan,&TxHeader,TxData5,&TxMailbox);//启动
}

void fun19(void) // can 空白 第三行
{
    OLED_ShowString(0, 0, "  CAN Info3  ", 16, 1);
    OLED_ShowString(0,16,"Speed:     /rpm",16,1);
    OLED_ShowString(0, 32, "Current:    /mA", 16, 1);
    HAL_Delay(10);
    OLED_ShowNum(51,16,Data_Speeed[4]+Data_Speeed[5]*256,4,16,1);
    OLED_ShowNum(64,32,Data_current[4]+Data_current[5]*256,4,16,1);
    HAL_Delay(10);
    if(KEY1_StateRead()==KEY_DOWN)//查询模式，必须在fun9函数执行期间按下去又弹上来。
    {
        TxData2[5]=TxData2[5]+1;
        HAL_CAN_AddTxMessage(&hcan,&TxHeader,TxData2,&TxMailbox);
    }
    
}
//485空白
void fun20(void) //485 空白 第一行
{
    OLED_ShowString(0, 0, "  485 Info1 ", 16, 1);
}

void fun21(void) //485 空白 第二行
{
    OLED_ShowString(0, 0, "  485 Info2 ", 16, 1);
}

void fun22(void) //485 空白 第三行
{
    OLED_ShowString(0, 0, "  485 Info3 ", 16, 1);
}
//uart 空白
void fun23(void) //uart 空白 第一行
{
    OLED_ShowString(0, 0, "  UART Info1 ", 16, 1);

}

void fun24(void) //uart 空白 第二行
{
    OLED_ShowString(0, 0, "  UART Info2 ", 16, 1);
}

void fun25(void) //uart 空白 第三行
{
    OLED_ShowString(0, 0, "  UART Info3 ", 16, 1);
}
